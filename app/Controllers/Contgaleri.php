<?php
namespace App\Controllers;

class Contgaleri extends BaseController
{
	function index($kodeProduk)
	{
		// Cek Data produk bedasarkan Kode Produk yang dikirim
		$paramProduct=array('kodeProduk'=>$kodeProduk);
		$rec=$this->objProduk->getDataBy($paramProduct)->getRow();

		$data['kodeProduk']=$rec->kodeProduk;
		$data['namaProduk']=$rec->namaproduk;
		$data['slugProduk']=$rec->slugProduk;

		$kriteriaGaleri=array('idFK'=>$kodeProduk);
		$data['dataGaleri']=$this->objGaleri->getDataBy($kriteriaGaleri);

		$data['admPage']='galeri-view';	
		return view("backend",$data);
	}
	
	function tambah_data($kodeProduk)
	{
		$paramProduct=array('kodeProduk'=>$kodeProduk);
		$rec=$this->objProduk->getDataBy($paramProduct)->getRow();

		$data['kodeProduk']=$rec->kodeProduk;
		$data['namaProduk']=$rec->namaproduk;
		$data['slugProduk']=$rec->slugProduk;

		$data['admPage']='galeri-form';
		return view("backend",$data);
	}

	function submit_form($kodeProduk=false,$idGaleri=false)
	{
		if($idGaleri!=false)
		{
			$paramGaleri=array('idGaleri'=>$idGaleri);
			$rec=$this->objGaleri->getDataBy($paramGaleri)->getRow();

			$data['idGaleri'] 			= $rec->idGaleri;
			$data['idFK']		 		= $rec->idFK;
			$data['title'] 				= $rec->title;
			$data['foto'] 				= $rec->foto;
			$data['alt'] 				= $rec->alt;
			$data['descriptionfoto'] 	= $rec->descriptionfoto;
		}

		
		// 1. Memeriksa apakah proses pengiriman data dari formulir dengan menekan tombol submit
		if($this->request->getMethod()=='post')
		{
			//2. Melakukan Validasi elemen form pada formulir

			// 2.1 Menentukan aturan pada setiap elemen form
			$rules=[
				'title'=>[
					'label' => 'Judul',
					'rules'	=> 'required|max_length[150]',
					'errors'=>[
						'required'   => 'Judul Foto tidak boleh kosong',
						'max_length' => 'Panjang karakter Judul Foto maksimal 10 karakter'
					]
				]
			];

			// 2.2 Memvalidasi/memeriksa aturan yang sudah dibuat

			if($this->validate($rules))
			{
				// 2.3 Upload Foto

				$path='./assets/img/galeri/detail/'; // posisi foto ukuran besar

				// Load library image untuk membuat foto menjadi ukuran kecil
				$images=service('image'); // library untuk manipulasi foto

				// menangkap semua file yang diupload pada elemen form benama uploadfile

				$files=$this->request->getFiles('uploadfile');

				$totalFile=count($files['uploadfile']);

				$noFile=1;

				foreach($files['uploadfile'] as $file)
				{
					if($noFile==1)
					{
						if($file->isValid() && !$file->hasMoved())
						{
							// Perintah untuk upload file
							$file->move('./assets/img/galeri/detail/', $file->getRandomName());

							// mendapatkan nama foto yang sudah dipload
							$fileName=$file->getName();

							// tahapan pembuatan foto thumbs
							// Bisa dihapus jika tidak diperlukan foto kecil
							$images->withFile($path.$fileName)
							->fit(150,150,'center')
							->save('./assets/img/galeri/thumb/'.$fileName);

							//hapus foto lama

							$namafoto=$this->request->getPost('foto');

							if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/galeri/detail/'.$namafoto)))
							{
								unlink(realpath(APPPATH . '../assets/img/galeri/detail/'.$namafoto));
							}

							if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/galeri/thumb/'.$namafoto)))
							{
								unlink(realpath(APPPATH . '../assets/img/galeri/thumb/'.$namafoto));
							}

						}
						else
						{
							if($this->request->getPost('foto')!='')
							{
								$fileName=$this->request->getPost('foto');
							}
							else
							{
								$fileName='no-image.jpg';
							}
						}
					}
				}

				// 2.4 Mempersiapkan array nilai yang mau disimpan ke tabel

				$data_save=array(
					'idGaleri'			=> $this->request->getPost('idGaleri'),
					'idFK'				=> $this->request->getPost('kodeProduk'),
					'foto'				=> $fileName,
					'alt'				=> $this->request->getPost('alt'),
					'title'				=> $this->request->getPost('title'),
					'descriptionfoto'	=> $this->request->getPost('descriptionfoto'),
				);

				// Memanggil function pada model untuk menyimpan data

				$idGaleri=$this->objGaleri->save_data($data_save);
				
				$this->session->setFlashdata('message','Proses penyimpanan data berhasil');
				return redirect()->to(base_url().'dashboard/data-galeri/'.$this->request->getPost('kodeProduk'));

			}
			else
			{
					
				$paramProduct=array('kodeProduk'=>$this->request->getPost('kodeProduk'));
				$rec=$this->objProduk->getDataBy($paramProduct)->getRow();

				$data['kodeProduk']=$rec->kodeProduk;
				$data['namaProduk']=$rec->namaproduk;


				$data['admPage']='galeri-form';
				return view("backend",$data);
			} // if($this->validate($rules))


		} //if($this->request->getMethod()=='post')
		else
		{
			$paramProduct=array('kodeProduk'=>$kodeProduk);
			$rec=$this->objProduk->getDataBy($paramProduct)->getRow();

			$data['kodeProduk']=$rec->kodeProduk;
			$data['namaProduk']=$rec->namaproduk;

			$data['admPage']='galeri-form';
			return view("backend",$data);
		} // if($this->validate($rules))

	}

	function hapus($idGaleri)
	{
		$paramGaleri=array('idGaleri'=>$idGaleri);
		$rec=$this->objGaleri->getDataBy($paramGaleri)->getRow();

		$idFK=$rec->idFK;
		$namafoto=$rec->foto;
		$title=$rec->title;

		if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/galeri/detail/'.$namafoto)))
		{
			unlink(realpath(APPPATH . '../assets/img/galeri/detail/'.$namafoto));
		}

		if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/galeri/thumb/'.$namafoto)))
		{
			unlink(realpath(APPPATH . '../assets/img/galeri/thumb/'.$namafoto));
		}

		$namaProduk=$this->request->getPost('namaproduk');

		$kriteriahapus=array('idGaleri'=>$idGaleri);
		$this->objGaleri->delete_data($kriteriahapus);

		$this->session->setFlashdata('message','Proses'.
			$title.' berhasil');
		return redirect()->to(base_url().'dashboard/data-galeri/'.$idFK);
	}
}