<?php
namespace App\Controllers;

class Contcart extends BaseController
{
	function index()
	{
		$data['items']=$this->session->get('cart');
		$data['total']=$this->total();

		$data['page']='data-cart';
		return view("frontend",$data);
	}
    function insert_cart()
    {
		$kodeProduk=$this->request->getPost('kodeProduk');

		$paramProduct=array('kodeProduk'=>$kodeProduk);
		$rec=$this->objProduk->getDataBy($paramProduct)->getRow();

			$kodeProduk 		= $rec->kodeProduk;
			$namaproduk 		= $rec->namaproduk;
			$slugProduk			= $rec->slugProduk;
			$harga				= $rec->harga;
			$fotoProduk			= $rec->fotoProduk;

		if($this->request->getMethod()=='post')
		{
			//2. Melakukan Validasi elemen form pada formulir

			// 2.1 Menentukan aturan pada setiap elemen form
			$rules=[
				'qty'=>[
					'label' => 'Quantity Produk',
					'rules'	=> 'required|greater_than_equal_to[1]',
					'errors'=>[
						'required'   			=> 'Quantity produk tidak boleh kosong',
						'greater_than_equal_to' 	=> 'Pembelian Minimal 1 pcs'
					]
				]
			];

			if($this->validate($rules))
			{
			$qty=$this->request->getPost('kodeProduk');

				$item=array(
					'id' 	=>$kodeProduk,
					'name'	=>$namaproduk,
					'photo'	=>$fotoProduk,
					'price'	=>$harga,
					'qty'	=>$qty
				);

				if($this->session->has('cart'))
				{
					$checkValue=$this->check_item_cart($kodeProduk);
					$cart=$this->session->get('cart');

					if($checkValue==1)
					{
						$cart[md5($kodeProduk)]['qty']++;
					}
					else
					{
						//array_push($cart,array(md5($kodeProduk)=>$item));

						$cart[md5($kodeProduk)]=$item;
					}
				}
				else
				{
					$cart[md5($kodeProduk)]=$item;
				}

				
				$this->session->set('cart',$cart);

				return redirect()->to(base_url().'shopping-cart');
			}
			else
			{
				$data['validation']=$this->validator;
				// mempersiapkan nilai kriteria pengambilan data berdasarkan idProduct
				$kriteria=array('kodeProduk'=>$kodeProduk);
				$data['dataProductBy']=$this->objProduk->getDataBy($kriteria);


				// ambil data related product

				$itemProd=$data['dataProductBy']->getRow();

				$arrKriteriaPengambilan=array('kategori'=>$itemProd->kategori);
				$arrKriteriaExcludeId=array('kodeProduk'=>$kodeProduk);

				$data['dataRelated']=$this->objProduk->getRelatedData($arrKriteriaPengambilan,$arrKriteriaExcludeId);

				$data['page']='data-product';
				return view("frontend",$data);
			}
		}
		else
		{

		}
    }

	function check_item_cart($kodeProduk)
	{
		if($this->session->has('cart'))
		{
			$items=$this->session->get('cart');

			foreach($items as $index =>$item)
			{
				if($index==md5($kodeProduk))
				{
					return 1;
				}
			}

			return -1;
		}
		else
		{
			return -1;
		}
	}

	function total()
	{
		$total=0;

		if($this->session->has('cart'))
		{
			$items=$this->session->get('cart');
			foreach($items as $item)
			{
				$total+=$item['qty']*$item['price'];
			}
		}

		return $total;
	}

	function total_qty()
	{
		$total_qty=0;

		if($this->session->has('cart'))
		{
			$items=$this->session->get('cart');
			foreach($items as $item)
			{
				$total_qty+=$item['qty'];
			}
		}

		return $total_qty;
	}

	function update_qty()
	{
		$rowid=$this->request->getPost('rowid');
		$qty=$this->request->getPost('qty');
		$cart=$this->session->get('cart');
		$cart[$rowid]['qty']=$qty;

		$this->session->set('cart',$cart);
		return redirect()->to(base_url().'shopping-cart');

	}

	function del_item_cart($rowid)
	{
		$cart=$this->session->get('cart');

		// menghapus nilai array berdasarkan index tertentu
		unset($cart[$rowid]);
		$cart2=$this->session->set('cart',$cart);
		return redirect()->to(base_url().'shopping-cart');
	}

	function cancel()
	{
		$this->session->remove('cart');
		return redirect()->to(base_url().'shopping-cart');
	}

	function continue()
	{
		$data['items']=$this->session->get('cart');
		$data['total']=$this->total();


		$data['page']='data-continue';
		return view("frontend",$data);
	}
	function checkout()
	{
		if($this->session->has('cart'))
		{

			/*Awal penyimpanan Customer*/

			$name=$this->request->getPost('name');
			$email=$this->request->getPost('email');

			$password=rand();

			$customer=array(
				'idcustomer'=>'',
				'name'		=>$name,
				'email'		=>$email,
				'password'	=>md5($password)
			);

			$idcustomer=$this->objCustomer->saveData($customer);

			// Akhir penyimpanan Customer

			// Awal penyimpanan Order

			$arrSaveOrder=array(
				'idorder'		=>'',
				'orderdate'		=>date("Y-m-d"),
				'idcustomer'	=>$idcustomer,
				'qty'			=>$this->total_qty(),
				'total'			=>$this->total(),
				'status'		=>'order'
			);

			$idorder=$this->objOrder->saveData($arrSaveOrder);

			// Akhir Penyimpanan data order

			// Awal penyimpanan ke order detail

			$items=$this->session->get('cart');
			foreach($items as $item)
			{
				$arrDetailOrder=array(
				'iddetailorder'		=>'',
				'idorder'			=>$idorder,
				'idproduct'			=>$item['id'],
				'selling_price'		=>$item['price'],
				'subqty'			=>$item['qty'],
				'subtotal'			=>$item['price']*$item['qty']
				);

				$this->objOrderDetail->saveData($arrDetailOrder);
			}

			$this->session->remove('cart');

			$data['page']='data-order-confirmation';
			return view("frontend",$data);
		}
		else
		{
			return redirect()->to(base_url().'continue');
		}
	}
}