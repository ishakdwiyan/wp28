<?php
namespace App\Controllers;

class ContLogin extends BaseController
{

	function index()
	{
		return view("login-view");
	}
    function proses_login()
    {
        if($this->request->getMethod()=='post')
		{
			//2. Melakukan Validasi elemen form pada formulir

			// 2.1 Menentukan aturan pada setiap elemen form
			$rules=[
				'useremail'=>[
					'label' => 'E-mail',
					'rules'	=> 'required|valid_email',
					'errors'=>[
						'required'      => 'E-mail tidak boleh kosong',
                        'valid_email'   => 'Penulisan E-mail salah'
					]
				],
				'userpassword'=>[
                    'label' => 'Password',
					'rules'	=> 'required',
                    'errors'=>[
						'required'      => 'Password tidak boleh kosong'
					]
				]

			];

			// 2.2 Memvalidasi/memeriksa aturan yang sudah dibuat

			if($this->validate($rules))
			{
                $emailAdm=$this->request->getPost('useremail');
                $passAdm=$this->request->getPost('userpassword');

                
                $paramLogin=array(
                    'emailAdm'=> $emailAdm,
					'passwordAdm' =>md5($passAdm)
				);

				$countDataAdm=$this->objAdmin->countData($paramLogin);
				if($countDataAdm > 0) //jika ditemukan
				{
					$dataAdm=$this->objAdmin->getDataBy($paramLogin)->getRow();

					$idadmin=$dataAdm->idadmin;
					$namaAdm=$dataAdm->namaAdm;
					$emailAdm=$dataAdm->emailAdm;

					$newData=array(
						'idadmin'  	=> $idadmin,
						'namaAdm'	=> $namaAdm,
						'emailAdm'	=> $emailAdm,
						'login'	=> 		true
					);

					$this->session->set($newData);
					return redirect()->to(base_url().'/dashboard');
				}
				else
				{
					$this->session->setFlashdata('message','Email atau password yang anda masukkan belum ada');
					return redirect()->to(base_url().'admin/login');
				}
            }
            else
            {
                $data['validation']=$this->validator;
                return view ("login-view", $data);
            }
        }

    }
	function logout()
	{
		$newData=['idadmin', 'namaAdm', 'emailAdm', 'login'];
		$this->session->remove($newData);
		return redirect()->to(base_url().'/dashboard');
	}
}