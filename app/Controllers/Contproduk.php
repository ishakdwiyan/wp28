<?php
namespace App\Controllers;

class Contproduk extends BaseController
{

	function index()
	{
		if($this->session->get('login')!='true')
		{
			return redirect()->to(base_url().'admin/login');
		}
		else
		{
			$data['userlogin']=$this->session->get('namaAdm');
		}
		
		$data['dataProduk']=$this->objProduk->getAlldataJoinCat();
		$data['admPage']='produk-view';
		return view("backend",$data);
	}

	function tambah_data()
	{
		$data['dataKategori']=$this->objkategori->getAlldata();
		$data['admPage']='produk-form';
		return view("backend",$data);
	}

	function submit_form($kodeProduk=false)
	{

		if($kodeProduk!=false)
		{
			$paramProduct=array('kodeProduk'=>$kodeProduk);
			$rec=$this->objProduk->getDataBy($paramProduct)->getRow();

			$data['kodeProduk'] 		= $rec->kodeProduk;
			$data['namaproduk'] 		= $rec->namaproduk;
			$data['slugProduk'] 		= $rec->slugProduk;
			$data['idslug'] 			= $rec->idslug;
			$data['harga'] 				= $rec->harga;
			$data['kategori'] 			= $rec->kategori;
			$data['fotoProduk'] 		= $rec->fotoProduk;
			$data['infoProduk'] 		= $rec->infoProduk;
			$data['totalStok'] 			= $rec->totalStok;
			$data['totalReiew'] 		= $rec->totalReiew;
			$data['avgReview'] 			= $rec->avgReview;
			$data['titleSeoProduk'] 	= $rec->titleSeoProduk;
			$data['metaDescSeoProduk'] 	= $rec->metaDescSeoProduk;
			$data['sequence'] 			= $rec->sequence;
		}
		
		// 1. Memeriksa apakah proses pengiriman data dari formulir dengan menekan tombol submit

		if($this->request->getMethod()=='post')
		{
			//2. Melakukan Validasi elemen form pada formulir

			// 2.1 Menentukan aturan pada setiap elemen form
			$rules=[
				'namaproduk'=>[
					'label' => 'Nama Produk',
					'rules'	=> 'required|max_length[150]',
					'errors'=>[
						'required'   => 'Nama produk tidak boleh kosong',
						'max_length' => 'Panjang karakter nama produk maksimal 10 karakter'
					]
				],
				'slugProduk'=>[
					'rules'	=> 'required'
				]

			];

			// 2.2 Memvalidasi/memeriksa aturan yang sudah dibuat

			if($this->validate($rules))
			{
				// 2.3 Upload Foto

				$path='./assets/img/product/'; // posisi foto ukuran besar

				// Load library image untuk membuat foto menjadi ukuran kecil
				$images=service('image'); // library untuk manipulasi foto

				// menangkap semua file yang diupload pada elemen form benama uploadfile

				$files=$this->request->getFiles('uploadfile');

				$totalFile=count($files['uploadfile']);

				$noFile=1;

				foreach($files['uploadfile'] as $file)
				{
					if($noFile==1)
					{
						if($file->isValid() && !$file->hasMoved())
						{
							// Perintah untuk upload file
							$file->move('./assets/img/product', $file->getRandomName());

							// mendapatkan nama foto yang sudah dipload
							$fileName=$file->getName();

							// tahapan pembuatan foto thumbs
							// Bisa dihapus jika tidak diperlukan foto kecil
							$images->withFile($path.$fileName)
							->fit(150,150,'center')
							->save('./assets/img/productThumb/'.$fileName);

							//hapus foto lama

							$namafoto=$this->request->getPost('fotoProduk');

							if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/product/'.$namafoto)))
							{
								unlink(realpath(APPPATH . '../assets/img/product/'.$namafoto));
							}

							if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/productThumb/'.$namafoto)))
							{
								unlink(realpath(APPPATH . '../assets/img/productThumb/'.$namafoto));
							}

						}
						else
						{
							if($this->request->getPost('fotoProduk')!='')
							{
								$fileName=$this->request->getPost('fotoProduk');
							}
							else
							{
								$fileName='no-image.jpg';
							}
						}
					}
				}

				// 2.4 Mempersiapkan array nilai yang mau disimpan ke tabel

				// Slug Route

				$txtslug	=trim($this->request->getPost('slugProduk'));
				$linkslug	=str_replace(' ','-',$txtslug);
				$newslug	=strtolower($linkslug);
				
				$data_save=array(
					'kodeProduk'		=> $this->request->getPost('kodeProduk'),
					'namaproduk'		=> $this->request->getPost('namaproduk'),
					'slugProduk'		=> $newslug,
					'idslug'			=> $this->request->getPost('idslug'),
					'harga'				=> $this->request->getPost('harga'),
					'kategori'			=> $this->request->getPost('kategori'),
					'fotoProduk'		=> $fileName,
					'infoProduk'		=> $this->request->getPost('infoProduk'),
					'totalStok'			=> $this->request->getPost('totalStok'),
					'totalReiew'		=> $this->request->getPost('totalReiew'),
					'avgReview'			=> $this->request->getPost('avgReview'),
					'titleSeoProduk'	=> $this->request->getPost('titleSeoProduk'),
					'metaDescSeoProduk'		=> $this->request->getPost('metaDescSeoProduk'),
					'sequence'		=> 0,

				);

				// Memanggil function pada model untuk menyimpan data

				$kodeProduk=$this->objProduk->save_data($data_save);
				
				// Memeriksa apakah idslug dari formulir ditemukan pada tblroute?
				$kriteriacekroute=array('idroute'=>$this->request->getPost('idslug'));
				$countRoute=$this->objRoute->countData($kriteriacekroute);

				if($countRoute > 0)  // Jika ditemukan
				{	
					// Nilai variable currentidroute diisi dengan idslug dari form
					$currentidRoute=$this->request->getPost('idslug');
				}
				else // Jika tidak
				{
					// Setting default currentidroute menjadi kosong
					$currentidRoute='';
				}

				// Penyimpanan ke tblroute
				$arrRoute=array(
					'idroute'		=> $currentidRoute,
					'routeSlug' 	=> $newslug,
					'routeTarget'	=> 'Contfront::data_product/'.$kodeProduk
				);
				$idroute=$this->objRoute->save_data($arrRoute);

				
				// Update Data Produk
				$arrDataUpdate=array(
					'kodeProduk'=> $kodeProduk,
					'idslug'	=> $idroute
				);
				$this->objProduk->save_data($arrDataUpdate);


				$this->session->setFlashdata('message','Proses penyimpanan data berhasil');
				return redirect()->to(base_url().'dashboard/data-produk');

			}
			else
			{
				$data['dataKategori']=$this->objkategori->getAlldata();
				$data['admPage']='produk-form';
				return view("backend",$data);
			} // if($this->validate($rules))


		} //if($this->request->getMethod()=='post')
		else
		{
			$data['dataKategori']=$this->objkategori->getAlldata();
			$data['admPage']='produk-form';
			return view("backend",$data);
		} // if($this->validate($rules))


	}


	function hapus($kodeProduk)
	{

		//hapus foto lama

		$paramProduct=array('kodeProduk'=>$kodeProduk);
		$rec=$this->objProduk->getDataBy($paramProduct)->getRow();


		$namafoto=$rec->fotoProduk;
		$namaProduk=$rec->namaproduk;

		if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/product/'.$namafoto)))
		{
			unlink(realpath(APPPATH . '../assets/img/product/'.$namafoto));
		}

		if($namafoto!="" and $namafoto!="no-image.jpg" and file_exists(realpath(APPPATH . '../assets/img/productThumb/'.$namafoto)))
		{
			unlink(realpath(APPPATH . '../assets/img/productThumb/'.$namafoto));
		}

		// Membuat kriteria penghapusan data
		$kriteriaHapus=array('kodeProduk'=>$kodeProduk);
		$this->objProduk->delete_data($kriteriaHapus);

		$this->session->setFlashdata('message','Proses penghapusan data '.
			$namaProduk.' berhasil');
		return redirect()->to(base_url().'dashboard/data-produk');

	}

}