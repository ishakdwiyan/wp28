<?php
namespace App\Controllers;

class Contback extends BaseController
{
	function index()
	{
		if($this->session->get('login')!='true')
		{
			return redirect()->to(base_url().'admin/login');
		}
		else
		{
			$data['userlogin']=$this->session->get('namaAdm');
		}

		$data['admPage']='adm-home';
		return view("backend",$data);
	}

}