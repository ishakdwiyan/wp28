<?php
namespace App\Controllers;

class Contfront extends BaseController
{
	function index()
	{

		$data['allDataProduk']=$this->objProduk->getAlldata();

		$data['page']='data-home';
		return view("frontend",$data);
	}

	function data_product($idProduk)
	{
		// mempersiapkan nilai kriteria pengambilan data berdasarkan idProduct
		$kriteria=array('kodeProduk'=>$idProduk);
		$data['dataProductBy']=$this->objProduk->getDataBy($kriteria);


		// ambil data related product

		$itemProd=$data['dataProductBy']->getRow();

		$arrKriteriaPengambilan=array('kategori'=>$itemProd->kategori);
		$arrKriteriaExcludeId=array('kodeProduk'=>$idProduk);

		$data['dataRelated']=$this->objProduk->getRelatedData($arrKriteriaPengambilan,$arrKriteriaExcludeId);

		$data['page']='data-product';
		return view("frontend",$data);
	}
}