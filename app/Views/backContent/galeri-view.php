<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Gallery <?php echo isset($namaProduk)?$namaProduk:''; ?></h1>
    
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item">
                <?php
                    echo anchor('dashboard/data-produk', 'Produk');
                ?>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
    </nav>

    <?php
        // load library session dari view
        $this->session = \Config\Services::session();

        // Menangkap nilai session
        $pesan=$this->session->getFlashdata('message');

        if($pesan)
        {
            echo '<p class="bg-gradient-warning p-1">'.$pesan.'</p>';
        }

    ?>
   
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <?php echo anchor('dashboard/data-galeri/tambah/'.$kodeProduk, 'Tambah Data',array('class'=>'btn btn-info')); ?>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Judul</th>
                            <th>ALT</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Judul</th>
                            <th>ALT</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    	<?php 
                    		$no=1;
                    		foreach($dataGaleri->getResult() as $itemgaleri)
                    		{?>
                    			<tr>
		                            <td style="text-align: center;"><?php echo $no; ?></td>
		                            <td>
		                            	<img src="<?php echo base_url();?>/assets/img/galeri/thumb/<?php echo $itemgaleri->foto; ?>" alt="" width="60">
		                            </td>
		                            <td><?php echo $itemgaleri->title; ?></td>
		                            <td><?php echo $itemgaleri->alt; ?></td>
		        
		                            <td>
		                            	<?php echo anchor('dashboard/data-galeri/ubah/'.$itemgaleri->idFK .'/'.$itemgaleri->idGaleri, 'Ubah',array('class'=>'btn btn-warning')); ?>

		                            	<?php echo anchor('dashboard/data-galeri/hapus/'.$itemgaleri->idGaleri, 'Hapus',array('class'=>'btn btn-danger','onclick'=>'return confirm("apakah anda yakin mau manghapus?")')); ?>
		                            </td>
		                        </tr>
                    		<?php
                    		$no++;
                    		}

                    	?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
