<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Form Gallery</h1>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item">
                <?php
                    echo anchor('dashboard/data-produk', 'Produk');
                ?>
            </li>
            <li class="breadcrumb-item">
                <?php
                    echo anchor('dashboard/data-galeri/'.$kodeProduk, $namaProduk);
                ?>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Form Gallery</li>
        </ol>
    </nav>
    <hr>

    <?php 
    // menampilkan pesan error secara keseluruhan
    echo isset($validation)?$validation->listErrors():'';
    ?>

    <form action="<?php echo base_url(); ?>dashboard/data-galeri/simpan" method="post" enctype="multipart/form-data">

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idtitle">Judul</label>
    		<div class="col-sm-6">
			
				<input type="hidden" class="form-control" name="kodeProduk" value="<?php echo isset($kodeProduk)?$kodeProduk:
					set_value('kodeProduk'); ?>">

                <input type="hidden" class="form-control" name="idGaleri" value="<?php echo isset($idGaleri)?$idGaleri:
					set_value('idGaleri'); ?>">

    			<input type="text" class="form-control" id="idtitle" name="title" value="<?php echo isset($title)?$title:
					set_value('title'); ?>">

                <?php 
                    if (isset($validation) && $validation->hasError('title')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('title').'</p>';
                    }
                 ?>
    		</div>
    	</div>


    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idfoto">Foto</label>
    		<div class="col-sm-4">
    			<input type="file" class="form-control" id="idfoto" name="uploadfile[]">	
    		</div>
    	</div>

		<?php 
		if(isset($foto))
			{
				?>
				<div class="row mb-3">
				<label class="col-sm-2 col-form-label"></label>
					<div class="col-sm-4">
						<img src="<?php echo base_url();?>/assets/img/galeri/thumb/<?php echo $foto; ?>" alt="" width="60">

						<input type="hidden" class="form-control" name="foto"
						value="<?php echo isset($foto)?$foto:set_value('foto'); ?>">
					</div>
				</div>
				
				<?php
			}
		?>
    

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idalt">Alt</label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" id="idalt" name="alt"
				value="<?php echo isset($alt)?$alt:set_value('alt'); ?>">
    		</div>
    	</div>


    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="iddescriptionfoto">Description</label>
    		<div class="col-sm-10">
    			<textarea class="tinyMCE form-control" id="iddescriptionfoto" name="descriptionfoto" rows="7"><?php echo isset($descriptionfoto)?$descriptionfoto:set_value('descriptionfoto'); ?></textarea>
			
				<div class="button_set">
					<input type="button" onclick="toggleEditor('iddescriptionfoto'); 
						return false;" value="Toggle WYSIWYG" />
				</div>
    		</div>


    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label"></label>
    		<div class="col-sm-8">
    			<input type="submit" class="btn btn-info" name="submit" value="Simpan">
    		</div>
    	</div>

    </form>

 </div>