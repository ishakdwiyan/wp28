<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Dashboard Admin</h1>
    <p class="mb-4">Selamat datang <?php echo isset($userlogin)?$userlogin:''; ?> di halaman dashboard admin</p>
    <p>
    	Halaman ini diperuntukkan bagi admin yang sudah memiliki hak akses untuk memaintenance data di sistem. Jika ada kesulitan saat menggunakan halaman ini, silakan tonton video tutorial di bawah ini.
    </p>
    
</div>