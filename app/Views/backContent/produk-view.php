<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Produk</h1>

    <?php
        // load library session dari view
        $this->session = \Config\Services::session();

        // Menangkap nilai session
        $pesan=$this->session->getFlashdata('message');

        if($pesan)
        {
            echo '<p class="bg-gradient-warning p-1">'.$pesan.'</p>';
        }

    ?>
   
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <?php echo anchor('dashboard/data-produk/tambah', 'Tambah Data',array('class'=>'btn btn-info')); ?>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>Kategori</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    	<?php 
                    		$no=1;
                    		foreach($dataProduk->getResult() as $itemproduk)
                    		{?>
                    			<tr>
		                            <td style="text-align: center;"><?php echo $no; ?></td>
		                            <td>
		                            	<img src="<?php echo base_url();?>/assets/img/product/<?php echo $itemproduk->fotoProduk; ?>" alt="" width="60">
		                            </td>
		                            <td><?php echo $itemproduk->namaproduk; ?></td>
		                            <td><?php echo $itemproduk->namaKategori; ?></td>
		                            <td style="text-align: center;"><?php echo $itemproduk->harga; ?></td>
		                            <td style="text-align: center;">

		                            	<?php echo number_format($itemproduk->totalStok,2,',','.'); ?>
		                            		
		                            </td>
		                            <td>
		                            	<?php echo anchor('dashboard/data-galeri/'.$itemproduk->kodeProduk, 'Galeri',array('class'=>'btn btn-success')); ?>

		                            	<?php echo anchor('dashboard/data-produk/ubah/'.$itemproduk->kodeProduk, 'Ubah',array('class'=>'btn btn-warning')); ?>

		                            	<?php echo anchor('dashboard/data-produk/hapus/'.$itemproduk->kodeProduk, 'Hapus',array('class'=>'btn btn-danger','onclick'=>'return confirm("apakah anda yakin mau manghapus?")')); ?>
		                            </td>
		                        </tr>
                    		<?php
                    		$no++;
                    		}

                    	?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
