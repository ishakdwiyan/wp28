<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Form Produk</h1>
    <hr>

    <?php 
    // menampilkan pesan error secara keseluruhan
    echo isset($validation)?$validation->listErrors():'';
    ?>

    <form action="<?php echo base_url(); ?>dashboard/data-produk/simpan" method="post" enctype="multipart/form-data">

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idnama">Nama</label>
    		<div class="col-sm-6">
			
				<input type="hidden" class="form-control" name="kodeProduk" value="<?php echo isset($kodeProduk)?$kodeProduk:
					set_value('kodeProduk'); ?>">

				<input type="hidden" class="form-control" id="idslug" name="idslug" value="<?php echo isset($idslug)?$idslug:
					set_value('idslug'); ?>">

    			<input type="text" class="form-control" id="idnama" name="namaproduk" value="<?php echo isset($namaproduk)?$namaproduk:
					set_value('namaproduk'); ?>">

                <?php 
                    if (isset($validation) && $validation->hasError('namaproduk')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('namaproduk').'</p>';
                    }
                 ?>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idslug">Slug</label>
    		<div class="col-sm-6">
    			<input type="text" class="form-control" id="idslug" name="slugProduk" 
				value="<?php echo isset($slugProduk)?$slugProduk:set_value('slugProduk'); ?>">

                <?php 
                    if (isset($validation) && $validation->hasError('slugProduk')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('slugProduk').'</p>';
                    }
                 ?>

    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idharga">Harga</label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" id="idharga" name="harga" 
				value="<?php echo isset($harga)?$harga:set_value('harga'); ?>">

				<?php 
                    if (isset($validation) && $validation->hasError('harga')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('harga').'</p>';
                    }
                 ?>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idkategori">Kategori</label>
    		<div class="col-sm-4">
    			<select class="form-control" id="idkategori" name="kategori">
    				<?php 
    				foreach($dataKategori->getResult() as $itemKategori) 
    				{
						if(isset($kategori)&& $kategori==$itemKategori->idKategori)
						{
    						echo '<option value="'.$itemKategori->idKategori .'" selected="selected">'.$itemKategori->namaKategori.'</option>';
						} 
						else
						{
							echo '<option value="'.$itemKategori->idKategori .'">'.$itemKategori->namaKategori.'</option>';
						}

					}
					?>
    				
    			</select>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idfoto">Foto</label>
    		<div class="col-sm-4">
    			<input type="file" class="form-control" id="idfoto" name="uploadfile[]">

				
    		</div>
    	</div>

		<?php 
		if(isset($fotoProduk))
			{
				?>
				<div class="row mb-3">
				<label class="col-sm-2 col-form-label"></label>
					<div class="col-sm-4">
						<img src="<?php echo base_url();?>/assets/img/product/<?php echo $fotoProduk; ?>" alt="" width="60">

						<input type="hidden" class="form-control" name="fotoProduk"
						value="<?php echo isset($fotoProduk)?$fotoProduk:set_value('fotoProduk'); ?>">
					</div>
				</div>
				
				<?php
			}
		?>
    

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idtotalStok">Total Stok</label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" id="idtotalStok" name="totalStok"
				value="<?php echo isset($totalStok)?$totalStok:set_value('totalStok'); ?>">

				<?php 
                    if (isset($validation) && $validation->hasError('totalStok')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('totalStok').'</p>';
                    }
                 ?>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idtotalReiew">Total Review</label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" id="idtotalReiew" name="totalReiew"
				value="<?php echo isset($totalReiew)?$totalReiew:set_value('totalReiew'); ?>">

				<?php 
                    if (isset($validation) && $validation->hasError('totalReiew')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('totalReiew').'</p>';
                    }
                 ?>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idavgReview">Rata-rata Review</label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" id="idavgReview" name="avgReview"
				value="<?php echo isset($avgReview)?$avgReview:set_value('avgReview'); ?>">

				<?php 
                    if (isset($validation) && $validation->hasError('avgReview')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('avgReview').'</p>';
                    }
                 ?>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idinfoProduk">Info Produk</label>
    		<div class="col-sm-10">
    			<textarea class="tinyMCE form-control" id="idinfoProduk" name="infoProduk" rows="7"><?php echo isset($infoProduk)?$infoProduk:set_value('infoProduk'); ?></textarea>
			
				<div class="button_set">
					<input type="button" onclick="toggleEditor('idinfoProduk'); 
						return false;" value="Toggle WYSIWYG" />
				</div>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idtitleSeoProduk">Title Seo Produk</label>
    		<div class="col-sm-6">
    			<input type="text" class="form-control" id="idtitleSeoProduk" name="titleSeoProduk"
				value="<?php echo isset($titleSeoProduk)?$titleSeoProduk:set_value('titleSeoProduk'); ?>">

				<?php 
                    if (isset($validation) && $validation->hasError('titleSeoProduk')) {
                        echo '<p class="bg-danger text-white mt-2 p-1">'.$validation->getError('titleSeoProduk').'</p>';
                    }
                 ?>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idinfoProduk">Meta Description</label>
    		<div class="col-sm-8">
    			<textarea class="form-control" id="idmetaDescSeoProduk" name="metaDescSeoProduk" rows="2"><?php echo isset($metaDescSeoProduk)?$metaDescSeoProduk:set_value('metaDescSeoProduk'); ?></textarea>
    		</div>
    	</div>

    	<div class="row mb-3">
    		<label class="col-sm-2 col-form-label" for="idinfoProduk"></label>
    		<div class="col-sm-8">
    			<input type="submit" class="btn btn-info" name="submit" value="Simpan">
    		</div>
    	</div>

    </form>

 </div>