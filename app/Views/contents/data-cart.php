    <!-- Page info -->
    <div class="page-top-info">
		<div class="container">
			<h4>Cart List</h4>
			<div class="site-pagination">
				<a href="">Home</a> /
				<a href="">Cart List</a>
			</div>
		</div>
	</div>
	<!-- Page info end -->
    <section class="product-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table border="1" class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                            <th>Action</th>
                        </tr>

                        <?php if($items): ?>
                            <?php
                                $no=1;
                                foreach($items as $index => $itemCart)
                                {?>
                                    <tr>
                                        <td><?php echo $itemCart['id']; ?></td>
                                        <td><?php echo $itemCart['name']; ?></td>
                                        <td><?php echo $itemCart['price']; ?></td>
                                        <td>
                                            <form action="<?php echo base_url(); ?>/update-cart" method="post">
                                                <input type="number" name="qty" value="<?php echo $itemCart['qty']; ?>">
                                                <input type="hidden" name="rowid" value="<?php echo $index; ?>">
                                                <input type="submit" name="submit" value="Update">
                                            </form>
                                        </td>
                                        <td><?php echo $itemCart['qty']*$itemCart['price']; ?></td>
                                        <td><?php echo anchor('delete-cart/'.$index,'Delete'); ?></td>
                                    </tr>

                                <?php
                                $no++;
                                }
                            ?>
                        <?php endif ?>

                        <tr>
                            <th colspan="4">Total</th>
                            <th><?php echo isset($total)?$total:''; ?></th>
                            <th><?php echo anchor('cancel-cart','Cancel'); ?></th>
                        </tr>
                        <tr>
                            <th colspan="5">Total</th>
                            <th><?php echo anchor('continue','Continue'); ?></th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>    
    </section>

    	<!-- cart section end -->
	<section class="cart-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="cart-table">
						<h3>Your Cart</h3>
						<div class="cart-table-warp">
							<table>
							<thead>
								<tr>
                                    <th class="product-th">No</th>
									<th class="quy-th">Product</th>
									<th class="size-th">Quantity</th>
									<th class="total-th">Price</th>
								</tr>
							</thead>
							<tbody>
                            <?php if($items): ?>
                            <?php
                                $no=1;
                                foreach($items as $index => $itemCart)
                                {?>
								    <tr>
                                    
                                        <td class="product-col">
                                            <div class="pc-title">
                                            <td><?php echo $itemCart['id']; ?></td>
                                                <td><?php echo $itemCart['name']; ?></td>
                                                <td class="quy-col">
                                                    <div class="quantity">
                                                        <div class="pro-qty">
                                                            <input type="text" value="<?php echo $itemCart['qty']; ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><?php echo $itemCart['qty']*$itemCart['price']; ?></td>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                <?php
                                $no++;
                                }
                            ?>
                            
						</tbody>
                        
						</table>
						</div>
							<div class="total-cost">
								<h6>Total <span>$99.90</span></h6>
							</div>
						<?php endif ?>
					</div>
				</div>
				<div class="col-lg-4 card-right">
					<form class="promo-code-form">
						<input type="text" placeholder="Enter promo code">
						<button>Submit</button>
					</form>
					<a href="" class="site-btn">Proceed to checkout</a>
					<a href="" class="site-btn sb-dark">Continue shopping</a>
				</div>
			</div>
		</div>
	</section>
	<!-- cart section end -->

	<!-- Related product section -->
	<section class="related-product-section">
		<div class="container">
			<div class="section-title text-uppercase">
				<h2>Continue Shopping</h2>
			</div>
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<div class="tag-new">New</div>
							<img src="./img/product/2.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Black and White Stripes Dress</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="./img/product/5.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Flamboyant Pink Top </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="./img/product/9.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Flamboyant Pink Top </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="./img/product/1.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Flamboyant Pink Top </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Related product section end -->
