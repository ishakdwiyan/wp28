    <!-- Page info -->
    <div class="page-top-info">
		<div class="container">
			<h4>Cart List</h4>
			<div class="site-pagination">
				<a href="">Home</a> /
				<a href="">Cart List</a>
			</div>
		</div>
	</div>
	<!-- Page info end -->
    <section class="product-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                   <table border="1" class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                    </tr>

                    <?php if($items): ?>
                        <?php
                            $no=1;
                            foreach($items as $index => $itemCart)
                            {?>
                                <tr>
                                    <td><?php echo $itemCart['id']; ?></td>
                                    <td><?php echo $itemCart['name']; ?></td>
                                    <td><?php echo $itemCart['price']; ?></td>
                                    <td>
                                        <?php echo $itemCart['qty']; ?>
                                    </td>
                                    <td><?php echo $itemCart['qty']*$itemCart['price']; ?></td>
                                    
                                </tr>

                            <?php
                            $no++;
                            }
                        ?>
                    <?php endif ?>

                    <tr>
                        <th colspan="4">Total</th>
                        <th><?php echo isset($total)?$total:''; ?></th>
                    </tr>
                    
                </table>

                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h3>Data Konsumen</h3>
                </div>
            </div>

           
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    
                <form method="post" action="<?php echo base_url(); ?>/checkout">
                        
                        <div class="row mb-3">
                            <label for="idname" class="col-md-2 form-label">Nama</label>
                            <div class="col-md-8">
                                <input type="text" name="name" id="idname" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="idemail" class="col-md-2 form-label">E-mail</label>
                            <div class="col-md-8">
                                <input type="email" name="email" id="idemail" class="form-control">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label  class="col-md-2 form-label"></label>
                            <div class="col-md-8">
                                <input type="submit" name="submit" class="btn btn-info">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>    
</section>