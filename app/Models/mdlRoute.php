<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlRoute extends Model
{
	protected $tbl="tblroute";
	protected $primary="idroute";

	protected $builder; // objek untuk berinteraksi dengan tabel
	protected $db; // objek untuk mengkoneksikan dengan database dan mengaktifkan library database

	function __Construct()
	{
		$this->db= \Config\Database::connect();
		$this->builder=$this->db->table($this->tbl);
	}

	function getdataBy($param)
	{
        $this->builder->where($param);
        return $this->builder->get();

	}
    function countData($param)
    {
        $this->builder->where($param);
        return $this->builder->countAllResults();
        // Menghitung Berapa Banyak record yang didapat dari query field tabel kita
    }

    function save_data($arrSave)
	{
		if($arrSave['idroute']>0)
		{
			$this->builder->where('idroute', $arrSave['idroute']);
			$this->builder->update($arrSave);
			//update .. where idroute=nilai kode
			
			return $arrSave['idroute'];
		}
		else
		{
		
		// Perintah memasukkan nilai ke tabel
		 $this->builder->insert($arrSave);

		// Perintah untuk menangkap ID nilai yang baru dimasukkan
        return $this->db->insertID();
		}
	}

	function delete_data($kriteriaHapus)
	{
		$this->builder->where($kriteriaHapus);
		$this->builder->delete();
	}
}