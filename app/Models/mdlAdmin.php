<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlAdmin extends Model
{
	protected $tbl="tbladmin";
	protected $primary="idadmin";

	protected $builder; // objek untuk berinteraksi dengan tabel
	protected $db; // objek untuk mengkoneksikan dengan database dan mengaktifkan library database

	function __Construct()
	{
		$this->db= \Config\Database::connect();
		$this->builder=$this->db->table($this->tbl);
	}

	function getAlldata()
	{
		return $this->builder->get();
		// select * from tblProduk 
	}
	function getDataBy($arrKriteria) 
	{
		$this->builder->where($arrKriteria);
		return $this->builder->get();
		// select * from tblProduk where ...
	}
	function countData($param)
    {
        $this->builder->where($param);
        return $this->builder->countAllResults();
    }
}