<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlKategori extends Model
{
	protected $tbl="tblkategori";
	protected $primary="idKategori";

	protected $builder; // objek untuk berinteraksi dengan tabel
	protected $db; // objek untuk mengkoneksikan dengan database dan mengaktifkan library database

	function __Construct()
	{
		$this->db= \Config\Database::connect();
		$this->builder=$this->db->table($this->tbl);
	}

	function getAlldata()
	{
		return $this->builder->get();
		// select * from tblkategori 
	}
}