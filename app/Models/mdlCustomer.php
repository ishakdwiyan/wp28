<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlcustomer extends Model
{
	protected $table      = 'tblcust'; // menentukan nama tabel jika diakses dari model ini
    protected $primaryKey = 'idcustomer'; // id produk yang memiliki settingan primary key

    protected $builder;
    protected $db;

    function __construct()
    {
    	$this->db      = \Config\Database::connect();
		$this->builder = $this->db->table('tblcust');
    }

    function saveData($arrSave)
    {
        if($arrSave['idcustomer']>0)
        {
            $this->builder->where('idcustomer',$arrSave['idcustomer']);
            $this->builder->update($arrSave);
            return $arrSave['idcustomer'];
        }
        else
        {
            $this->builder->insert($arrSave);
            return $this->db->insertID();
        }
    }
}