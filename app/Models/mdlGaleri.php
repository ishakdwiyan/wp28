<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlGaleri extends Model
{
	protected $tbl="tblgaleri";
	protected $primary="idGaleri";

	protected $builder; // objek untuk berinteraksi dengan tabel
	protected $db; // objek untuk mengkoneksikan dengan database dan mengaktifkan library database

	function __Construct()
	{
		$this->db= \Config\Database::connect();
		$this->builder=$this->db->table($this->tbl);
	}

	function getAlldata()
	{
		return $this->builder->get();
		// select * from tblProduk 
	}

	function getDataBy($arrKriteria) //$arrKriteria variable yang dibuat sendiri unutk menangkap nilai pemanggilnya
	{
		$this->builder->where($arrKriteria);
		return $this->builder->get();
		// select * from tblProduk where ...
	}


	function save_data($arrSave)
	{
		if($arrSave['idGaleri']>0)
		{
			$this->builder->where('idGaleri', $arrSave['idGaleri']);
			$this->builder->update($arrSave);
			//update .. where idGaleri=nilai kode
			
			return $arrSave['idGaleri'];
		}
		else
		{
		
		// Perintah memasukkan nilai ke tabel
		 $this->builder->insert($arrSave);

		// Perintah untuk menangkap ID nilai yang baru dimasukkan
        return $this->db->insertID();
		}
	}


	function delete_data($kriteriaHapus)
	{
		$this->builder->where($kriteriaHapus);
		$this->builder->delete();
	}


	function getRelatedData($arrKriteriaPengambilan,$arrKriteriaExcludeId)
	{
		$this->builder->where($arrKriteriaPengambilan);
		$this->builder->where($arrKriteriaExcludeId);
		return $this->builder->get();

		// select * from tblProduk where kategori=1 and idProduk!=2
	}


}