<?php
namespace App\Models;

use CodeIgniter\Model;

class Mdlorderdetail extends Model
{
	protected $table      = 'tbldetailorder'; // menentukan nama tabel jika diakses dari model ini
    protected $primaryKey = 'iddetailorder'; // id produk yang memiliki settingan primary key

    protected $builder;
    protected $db;

    function __construct()
    {
    	$this->db      = \Config\Database::connect();
		$this->builder = $this->db->table('tbldetailorder');
    }

    function getOrderDetailBy($param)
    {
        $this->builder->select('*');
        $this->builder->join('product','product.idproduct =tbldetailorder.idproduct ');
        $this->builder->where($param);
        return $this->builder->get();
    }

    function saveData($arrSave)
    {
        if($arrSave['iddetailorder']>0)
        {
            $this->builder->where('iddetailorder',$arrSave['iddetailorder']);
            $this->builder->update($arrSave);
            return $arrSave['iddetailorder'];
        }
        else
        {
            $this->builder->insert($arrSave);
            return $this->db->insertID();
        }
    }
}