<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlProduk extends Model
{
	protected $tbl="tblproduk";
	protected $primary="kodeProduk";

	protected $builder; // objek untuk berinteraksi dengan tabel
	protected $db; // objek untuk mengkoneksikan dengan database dan mengaktifkan library database

	function __Construct()
	{
		$this->db= \Config\Database::connect();
		$this->builder=$this->db->table($this->tbl);
	}

	function getAlldata()
	{
		return $this->builder->get();
		// select * from tblProduk 
	}

	function getAlldataJoinCat()
	{
		$this->builder->select("*");
		$this->builder->join('tblkategori','tblkategori.idKategori=tblproduk.kategori');
		return $this->builder->get();
	}

	function getDataBy($arrKriteria) //$arrKriteria variable yang dibuat sendiri unutk menangkap nilai pemanggilnya
	{
		$this->builder->where($arrKriteria);
		return $this->builder->get();
		// select * from tblProduk where ...
	}


	function save_data($arrSave)
	{
		if($arrSave['kodeProduk']>0)
		{
			$this->builder->where('kodeProduk', $arrSave['kodeProduk']);
			$this->builder->update($arrSave);
			//update .. where kodeproduk=nilai kode
			
			return $arrSave['kodeProduk'];
		}
		else
		{
		
		// Perintah memasukkan nilai ke tabel
		 $this->builder->insert($arrSave);

		// Perintah untuk menangkap ID nilai yang baru dimasukkan
        return $this->db->insertID();
		}
	}


	function delete_data($kriteriaHapus)
	{
		$this->builder->where($kriteriaHapus);
		$this->builder->delete();
	}


	function getRelatedData($arrKriteriaPengambilan,$arrKriteriaExcludeId)
	{
		$this->builder->where($arrKriteriaPengambilan);
		$this->builder->where($arrKriteriaExcludeId);
		return $this->builder->get();

		// select * from tblProduk where kategori=1 and idProduk!=2
	}


}