<?php
namespace App\Models;

use CodeIgniter\Model;

class mdlorder extends Model
{
	protected $table      = 'tblorder'; // menentukan nama tabel jika diakses dari model ini
    protected $primaryKey = 'idorder'; // id produk yang memiliki settingan primary key

    protected $builder;
    protected $db;

    function __construct()
    {
    	$this->db      = \Config\Database::connect();
		$this->builder = $this->db->table('tblorder');
    }

    function getAllOrder()
    {
    	$this->builder->select('*');
        $this->builder->join('customer','customer.idcustomer=tblorder.idcustomer');
        
        return $this->builder->get();
    }

    function getAllOrderBy($param)
    {
        $this->builder->select('*');
        $this->builder->join('customer','customer.idcustomer=tblorder.idcustomer');
        $this->builder->where($param);
        return $this->builder->get();
    }

    function saveData($arrSave)
    {
        if($arrSave['idorder']>0)
        {
            $this->builder->where('idorder',$arrSave['idorder']);
            $this->builder->update($arrSave);
            return $arrSave['idorder'];
        }
        else
        {
            $this->builder->insert($arrSave);
            return $this->db->insertID();
        }
    }
}