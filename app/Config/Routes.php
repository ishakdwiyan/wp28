<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Contfront::index');

$routes->get('/shopping-cart', 'Contcart::index');
$routes->post('/insert-cart', 'Contcart::insert_cart');
$routes->post('/update-cart', 'Contcart::update_qty');
$routes->get('/delete-cart/(:any)', 'Contcart::del_item_cart/$1');
$routes->get('/cancel-cart', 'Contcart::cancel');
$routes->get('/continue', 'Contcart::continue');
$routes->post('/checkout', 'Contcart::checkout');

// $routes->get('/flamboyant-pink-top', 'Contfront::data_product/1');
// $routes->get('/black-and-white-strips-dress', 'Contfront::data_product/2');
// $routes->get('/alana-top', 'Contfront::data_product/3');




// membuat koneksi host
$con=mysqli_connect('localhost', 'root', '');

// Koneksi Ke database
mysqli_select_db($con,'db28') or die ("Koneksi database gagal");

$query=mysqli_query($con,"select * from tblroute");

while($row=mysqli_fetch_array($query))
{
    $routes->get('/'.$row['routeSlug'], $row['routeTarget']);
}
// end

// login Route

$routes->get('/admin/login', 'ContLogin::index');   
$routes->post('/admin/proses-login', 'ContLogin::proses_login');   
$routes->get('admin/logout', 'ContLogin::logout');   






// Admin Routes

$routes->get('/dashboard', 'Contback::index');

$routes->get('dashboard/data-produk', 'Contproduk::index');
$routes->get('dashboard/data-produk/hapus/(:num)', 'Contproduk::hapus/$1');
$routes->get('dashboard/data-produk/tambah','Contproduk::tambah_data');
$routes->get('dashboard/data-produk/ubah/(:num)', 'Contproduk::submit_form/$1');
$routes->post('dashboard/data-produk/simpan','Contproduk::submit_form');

$routes->get('dashboard/data-galeri/(:num)', 'Contgaleri::index/$1');
$routes->get('dashboard/data-galeri/hapus/(:num)', 'Contgaleri::hapus/$1');
$routes->get('dashboard/data-galeri/tambah/(:num)','Contgaleri::tambah_data/$1');
$routes->get('dashboard/data-galeri/ubah/(:num)/(:num)', 'Contgaleri::submit_form/$1/$2');
$routes->post('dashboard/data-galeri/simpan','Contgaleri::submit_form');

